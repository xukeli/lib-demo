#![allow(dead_code)]
#![allow(unused_variables)]

use std::{cell::Cell, marker::PhantomPinned, pin::Pin, ptr::NonNull, future::Future};

#[derive(Debug)]
struct Foo {
    foo: u64,
}

impl Drop for Foo {
    fn drop(&mut self) {
        println!("drop invoke for Foo: {:?}, foo: {}", self, self.foo);
    }
}

#[test]
fn drop_test() {
    let mut v = vec![Foo { foo: 1 }, Foo { foo: 2 }, Foo { foo: 3 }];
    v[2] = Foo { foo: 1145141919810 };
}

/// 使用Pin创建自引用字段结构体, 代价是不能用std::mem的方法改变值的地址
struct Unmovable {
    data: String,
    slice: NonNull<String>,
    _pin: PhantomPinned,
}
impl Unmovable {
    fn new(data: String) -> Pin<Box<Self>> {
        let res = Unmovable {
            data,
            // 只有在数据到位时，才创建指针，否则数据会在开始之前就被转移所有权
            slice: NonNull::dangling(),
            _pin: PhantomPinned,
        };
        let mut boxed = Box::pin(res);

        let slice = NonNull::from(&boxed.data);
        // 这里其实安全的，因为修改一个字段不会转移整个结构体的所有权
        unsafe {
            let mut_ref: Pin<&mut Self> = Pin::as_mut(&mut boxed);
            Pin::get_unchecked_mut(mut_ref).slice = slice;
        }
        boxed
    }
}

#[derive(Debug)]
struct Bar<'a> {
    other: Cell<Option<&'a Bar<'a>>>,
}

impl<'a> Drop for Bar<'a> {
    fn drop(&mut self) {
        println!("Bar droped!");
    }
}

#[test]
fn cycle() {
    let a = Bar {
        other: Cell::new(None),
    };
    let b = Bar {
        other: Cell::new(None),
    };
    // a.other.set(Some(&b));
    // b.other.set(Some(&a));
    // println!("{:?}", a);
}

trait TraitStatic: Send + 'static {
    fn foo(&self);
}

#[derive(Debug)]
struct StructStatic<'a> {
    l: u64,
    s: &'a mut u64,
}

impl TraitStatic for StructStatic<'static> {
    fn foo(&self) {
        println!("foo{}", self.s);
    }
}

static mut I: u64 = 5;
static J: i32 = 233;

#[test]
fn static_test() {
    {
        // let mut s: u64 = 5;
        // let sr = &mut s;
        let sr = unsafe { &mut I };
        unsafe { I += 1; }
        let ss = StructStatic { l: 1, s: sr };
        ss.foo();
    }
    println!("ooooooooooooooooooo");
    let f_ft = f();
}

async fn f() {

}

fn gen_fn_clj() -> impl Fn(i32) -> i32 {
    |x| x + J + 1
}

fn gen_fn() -> fn(i32) -> i32 {
    |x| x + 1
}

trait AAA {
    type GetUser<'s>: Future<Output=()> + 's where Self: 's;
    fn get_user(&self) -> Self::GetUser<'_>;
}

struct BBB;

type IntVec = Vec<i64>;

impl AAA for BBB {
    type GetUser<'s> = Pin<Box<dyn Future<Output=()>>>;

    fn get_user(&self) -> Self::GetUser<'_> {
        Box::pin(async {
            ()
        })
    }
}