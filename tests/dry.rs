#![allow(dead_code)]
#![allow(unused_variables)]
#![allow(unused_macros)]

//! 使用(声明)宏, 减少代码重复

use std::ops::{Add, Mul, Sub};

/// block
/// expr (is used for expressions)
/// ident (is used for variable/function names)
/// item
/// literal (is used for literal constants)
/// pat (pattern)
/// path
/// stmt (statement)
/// tt (token tree)
/// ty (type)
/// vis (visibility qualifier)

macro_rules! assert_equal_len {
    ($a:expr, $b:expr, $func:ident, $op:tt) => {
        assert!(
            $a.len() == $b.len(),
            "{:?}: dimension mismatch: {:?} {:?} {:?}",
            stringify!($func),
            $a.len(),
            stringify!($op),
            $b.len()
        );
    };
}

macro_rules! op {
    ($func:ident, $bound:ident, $op:tt, $method:ident) => {
        fn $func<T: $bound<T, Output=T> + Copy>(xs: &mut Vec<T>, ys: &Vec<T>) {
            assert_equal_len!(xs, ys, $func, $op);
            for (x, y) in xs.iter_mut().zip(ys.iter()) {
                *x = $bound::$method(*x, *y);
            }
        }
    };
}

op!(add_assign, Add, +=, add);
op!(mul_assign, Mul, *=, mul);
op!(sub_assign, Sub, -=, sub);

mod test {
    use std::iter;
    macro_rules! test {
        ($func:ident, $x:expr, $y:expr, $z:expr) => {
            #[test]
            fn $func() {
                for size in 0usize..10 {
                    let mut x: Vec<_> = iter::repeat($x).take(size).collect();
                    let y: Vec<_> = iter::repeat($y).take(size).collect();
                    let z: Vec<_> = iter::repeat($z).take(size).collect();

                    super::$func(&mut x, &y);
                    assert_eq!(x, z);
                }
            }
        };
    }

    test!(add_assign, 1u32, 2u32, 3u32);
    test!(mul_assign, 2u32, 3u32, 6u32);
    test!(sub_assign, 3u32, 2u32, 1u32);
}



#[test]
fn zip_test() {
    let mut xs = vec![1, 2, 3, 4];
    let ys = vec![2, 4, 8];
    let iter = xs.iter_mut().zip(ys.iter());
    for (x, y) in iter {
        println!("iter: {:?} -> {:?}", x, y);
    }
}


use std::collections::HashMap;
macro_rules! hash_map {
    ($($key:expr => $value:expr),*) => {{
        let mut hm = HashMap::new();
        $(hm.insert($key, $value);)*
        hm
    }};
}

fn map_macro() {
    let map = hash_map!(
        "foo" => 1,
        "bar" => 2
    );

}