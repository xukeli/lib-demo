#![allow(dead_code)]
#![allow(unused_variables)]

use kvs::util::now_str;

#[test]
fn format_test() {
    let foo = 1145141919810u64;
    let s = format!("{:X}", foo);
    println!("s: {}", s);
}

enum EnumType {
    OneStruct {
        a: i32,
        b: u32,
        c: f32,
        d: f64,
    },
    SecondNameless(i32, u32, f32, f64),
}

struct Number {
    value: i32,
}

#[test]
fn test_from() {
    let now = now_str();
    println!("{}", now);
}