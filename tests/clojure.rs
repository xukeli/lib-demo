#![allow(dead_code)]
#![allow(unused_variables)]

use std::{cell::RefCell, env::current_dir, fs};

use anyhow::Context;
use kvs::{util::now_str, config};

#[derive(Debug)]
struct Foo;

impl Foo {
    fn len(&self) -> usize {
        return 1;       
    }
    fn mut_consume(&mut self) {
        println!("consume Foo, {:#?}", self);
    }
}

fn fn_once<F>(func: F)
where
    F: FnOnce(usize) -> bool + Copy,
{
    println!("{}", func(3));
    println!("{}", func(4));
}

#[test]
fn once() {
    // let x = vec![1, 2, 3];
    let x = Foo{};
    fn_once(|z|{/*x.mut_consume();*/z == x.len()});
    println!("{:?}", x);

    // let arr = [Foo{}; 100];
}


#[test]
fn cell() {
    let a = RefCell::new(String::from("aaaaaaaaaaaaa"));
    let ab = a.borrow_mut();
    println!("{:?}", ab);
    drop(ab);
    let ac = a.borrow_mut();
    println!("{:?}", ac);
}

fn consume<T>(_t: T) {

}

#[test]
fn data_now() {
    let now = now_str();
    println!("{}", now);
}

#[test]
fn error() -> anyhow::Result<()> {
    let dir = current_dir()?.join("aosidnfpqwenfpoasijdf");
    let read_ret = fs::read_to_string(dir.clone()).with_context(move || format!("fail to read from path: {:?}", dir));
    match read_ret {
        Ok(str) => println!("{}",str),
        Err(err) => {
            let e = kvs::KvsError::from(err);
            println!("the error: {}", e);
        }
    }
    Ok(())
}

#[test]
fn error_log() -> anyhow::Result<()> {
    use tracing::error;
    config::init_log_config(true);
    let dir = current_dir()?.join("aosidnfpqwenfpoasijdf");
    let err = fs::read(dir.clone()).unwrap_err();
    error!(cause = ?err, ">>>>>>>>>>>>>error");
    Ok(())
}