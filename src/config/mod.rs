//! 配置

use std::{io, fmt};

use chrono::Local;
use tracing::Level;
use tracing_subscriber::fmt::{time::FormatTime, format};

// env_logger的配置方式
#[allow(dead_code)]
fn env_logger_config_init() {
    use std::io::Write;
    let env = env_logger::Env::default().filter_or(env_logger::DEFAULT_FILTER_ENV, "trace");
    env_logger::Builder::from_env(env)
        .format(|buf, record| {
            let level = { buf.default_styled_level(record.level()) };
            writeln!(
                buf,
                "{} {} [{}:{}:{}] {}",
                Local::now().format("%Y-%m-%d %H:%M:%S"),
                format_args!("{:>5}", level),
                record.module_path().unwrap_or("<unnamed>"),
                record.file().unwrap_or("<nofile>"),
                record.line().unwrap_or(0),
                &record.args()
            )
        })
        .init();
}

/// 日志配置初始化
pub fn init_log_config(output_stdout: bool) {
    let format = tracing_subscriber::fmt::format()
        .with_level(true)
        .with_target(true)
        .with_timer(LocalTimer)
        .with_source_location(true)
        .with_thread_ids(true)
        .with_thread_names(true);

    let builder = tracing_subscriber::fmt()
        .with_max_level(Level::TRACE)
        .event_format(format);
    if output_stdout {
        builder.with_writer(io::stdout).with_ansi(true).init();
    } else {
        let file_appender = tracing_appender::rolling::daily("./log", "tracing.log");
        let (non_blocking, _guard) = tracing_appender::non_blocking(file_appender);
        builder.with_writer(non_blocking).with_ansi(false).init();
    }
}

struct LocalTimer;

impl FormatTime for LocalTimer {
    fn format_time(&self, w: &mut format::Writer<'_>) -> fmt::Result {
        write!(w, "{}", Local::now().format("%Y-%m-%d %H:%M:%S%.6f"))
    }
}
