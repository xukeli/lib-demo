use thiserror::Error;
use std::{io, string::FromUtf8Error};

/// Error type for kvs.
#[derive(Error, Debug)]
pub enum KvsError {
    /// IO error.
    #[error("{source}")]
    Io {
        /// the source
        #[from]
        source: io::Error,
    },
    /// Serialization or deserialization error.
    #[error("{0}")]
    Serde(#[from] serde_json::Error),
    /// Removing non-existent key error.
    #[error("Key not found")]
    KeyNotFound,
    /// Unexpected command type error.
    /// It indicated a corrupted log or a program bug.
    #[error("Unexpected command type")]
    UnexpectedCommandType,
    /// Key or value is invalid UTF-8 sequence
    #[error("UTF-8 error: {0}")]
    Utf8(#[from] FromUtf8Error),
    /// Sled error
    #[error("sled error: {0}")]
    Sled(#[from] sled::Error),
    /// Error with a string message
    #[error("你是一个,一个一个一个一个一个错误啊啊啊啊啊啊啊: {0}")]
    StringError(String),
    /// 错误可以打印上下文信息
    #[error("{0}")]
    Any(#[from] anyhow::Error),
}

/// Result type for kvs.
pub type Result<T> = std::result::Result<T, KvsError>;
