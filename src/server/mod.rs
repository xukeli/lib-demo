//! doc

use crate::{
    common::{Request, Response},
    connection::Connection,
    KvsEngine, Result,
};
use tracing::{error, info, instrument};
use std::sync::Arc;
use tokio::{
    net::{TcpListener, TcpStream},
    signal,
    sync::{broadcast, mpsc, Semaphore},
};

const MAX_CONNECTIONS: usize = 250;

/// run the server
pub async fn run<E: KvsEngine>(listener: TcpListener, engine: E) {
    let mut server = KvsServer::new(engine, listener);

    tokio::select! {
        ret = server.run() => {
            if let Err(err) = ret {
                error!("failed to accept, err: {}", err);
            }
        }
        _ = signal::ctrl_c() => {
            info!("shutting down");
        }
    }

    let KvsServer {
        mut shutdown_complete_rx,
        shutdown_complete_tx,
        notify_shutdown,
        ..
    } = server;

    drop(notify_shutdown);
    drop(shutdown_complete_tx);

    let _ = shutdown_complete_rx.recv().await;
}

/// The server of a key value store.
pub struct KvsServer<E: KvsEngine> {
    listener: TcpListener,
    engine: E,
    limit_connections: Arc<Semaphore>,
    notify_shutdown: broadcast::Sender<()>,
    shutdown_complete_rx: mpsc::Receiver<()>,
    shutdown_complete_tx: mpsc::Sender<()>,
}

impl<E: KvsEngine> KvsServer<E> {
    /// Create a `KvsServer` with a given storage engine.
    pub fn new(engine: E, listener: TcpListener) -> Self {
        let (notify_shutdown, _) = broadcast::channel(1);
        let (shutdown_complete_tx, shutdown_complete_rx) = mpsc::channel(1);

        KvsServer {
            listener,
            engine,
            limit_connections: Arc::new(Semaphore::new(MAX_CONNECTIONS)),
            notify_shutdown,
            shutdown_complete_tx,
            shutdown_complete_rx,
        }
    }

    /// Run the server listening on the given address
    pub async fn run(&mut self) -> Result<()> {
        info!("accepting inbound connections");
        loop {
            self.limit_connections.acquire().await.unwrap().forget();
            let (tcp, _) = self.listener.accept().await?;
            self.serve(tcp);
        }
    }

    fn serve(&self, socket: TcpStream) {
        info!(">>>>>>accepted new connection");
        let mut handler = Handler {
            engine: self.engine.clone(),
            connection: Connection::new(socket),
            limit_connections: self.limit_connections.clone(),
            shutdown: Shutdown::new(self.notify_shutdown.subscribe()),
            _shutdown_complete: self.shutdown_complete_tx.clone(),
        };
        tokio::spawn(async move {
            if let Err(e) = handler.run().await {
                error!(cause = ?e, "connection error")
            }
        });
    }
}



#[derive(Debug)]
struct Handler<E: KvsEngine> {
    engine: E,
    connection: Connection,
    limit_connections: Arc<Semaphore>,
    shutdown: Shutdown,
    _shutdown_complete: mpsc::Sender<()>,
}

impl<E: KvsEngine> Handler<E> {
    #[instrument(skip(self))]
    async fn run(&mut self) -> Result<()> {
        info!(">>>>>>>>>>>>started run connection handler");
        while !self.shutdown.is_shutdown() {
            info!(">>>>>>before read request");
            let maybe_req = tokio::select! {
                req = self.connection.read_req() => req?,
                _ = self.shutdown.recv() => {
                    info!(">>>>>> received shutdown signal");
                    return Ok(());
                },
            };

            info!(">>>>>>read request complete");
            let resp = match maybe_req {
                Request::Get { key } => {
                    let get_future = self.engine.get(key);
                    get_future.await.map(Response::Get)
                }
                Request::Set { key, value } => {
                    let set_future = self.engine.set(key, value);
                    set_future.await.map(|_| Response::Set)
                }
                Request::Remove { key } => {
                    let rm_future = self.engine.remove(key);
                    rm_future.await.map(|_| Response::Remove)
                }
            };
            match resp {
                Ok(resp) => {
                    self.connection
                        .write_json(serde_json::to_string(&resp).unwrap().as_str())
                        .await?
                }
                Err(err) => {
                    let e = Response::Err(format!("{}", err));
                    self.connection
                        .write_json(serde_json::to_string(&e).unwrap().as_str())
                        .await?
                }
            }
        }

        Ok(())
    }
}

impl<E: KvsEngine> Drop for Handler<E> {
    fn drop(&mut self) {
        self.limit_connections.add_permits(1);
    }
}

#[derive(Debug)]
struct Shutdown {
    shutdown: bool,
    notify: broadcast::Receiver<()>,
}

impl Shutdown {
    fn new(notify: broadcast::Receiver<()>) -> Shutdown {
        Shutdown {
            shutdown: false,
            notify,
        }
    }

    fn is_shutdown(&self) -> bool {
        self.shutdown
    }

    async fn recv(&mut self) {
        if self.shutdown {
            return;
        }

        let received = self.notify.recv().await;
        info!("receive the shutdown broadcast message: {:?}", received);
        self.shutdown = true;
    }
}
