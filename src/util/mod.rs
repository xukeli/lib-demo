//! util function

use chrono::Local;

const DEFAULT_DATETIME_FMT: &'static str = "%Y-%m-%d %H:%M:%S";

/// 当前时间字符串, 默认格式化
pub fn now_str() -> String {
    Local::now().format(DEFAULT_DATETIME_FMT).to_string()
}

/// 当前时间字符串, 按照指定时间格式化
pub fn now_str_fmt(fmt: &str) -> String {
    Local::now().format(fmt).to_string()
}