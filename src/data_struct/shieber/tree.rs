//! 定义二叉树

// 子节点指针
type Link<T> = Option<Box<BinaryTree<T>>>;

/// 二叉树定义
#[derive(Debug, Clone)]
pub struct BinaryTree<T> {
    key: T,
    left: Link<T>,
    right: Link<T>,
}

impl<T: Clone> BinaryTree<T> {
    /// construct a new BinaryTree
    pub fn new(key: T) -> Self {
        BinaryTree {
            key,
            left: None,
            right: None,
        }
    }

    /// insert a new node as the left child tree from given tree
    /// 原来的左子节点变成新节点的左子节点
    pub fn insert_left_tree(&mut self, key: T) {
        if self.left.is_none() {
            let node = BinaryTree::new(key);
            self.left = Some(Box::new(node));
        } else {
            let mut node = BinaryTree::new(key);
            node.left = self.left.take();
            self.left = Some(Box::new(node));
        }
    }

    /// 同上但右
    pub fn insert_right_tree(&mut self, key: T) {
        if self.right.is_none() {
            let node = BinaryTree::new(key);
            self.right = Some(Box::new(node));
        } else {
            let mut node = BinaryTree::new(key);
            node.right = self.right.take();
            self.right = Some(Box::new(node));
        }
    }

    /// 插入或修改左子节点的key值
    pub fn replace_left_node(&mut self, key: T) {
        if self.left.is_none() {
            let node = BinaryTree::new(key);
            self.left = Some(Box::new(node));
        } else {
            let left = self.left.as_deref_mut().unwrap();
            left.key = key;
        }
    }

    /// 插入或修改右子节点key
    pub fn replace_right_node(&mut self, key: T) {
        if self.right.is_none() {
            let node = BinaryTree::new(key);
            self.right = Some(Box::new(node));
        } else {
            let right = self.right.as_deref_mut().unwrap();
            right.key = key;
        }
    }

    /// 这里clone会造成递归clone
    pub fn get_left(&self) -> Link<T> {
        self.left.clone()
    }

    /// 同上
    pub fn get_right(&self) -> Link<T> {
        self.right.clone()
    }

    /// get key
    pub fn get_key(&self) -> T {
        self.key.clone()
    }

    /// set key
    pub fn set_key(&mut self, key: T) {
        self.key = key;
    }
}

#[cfg(test)]
mod tests {


    use std::{mem, thread, time::Duration};

    use super::*;

    #[test]
    fn construct() {
        let mut bt = BinaryTree::new(1);
        assert_eq!(bt.key, 1);
        bt.insert_left_tree(3);
        {
            let left = bt.left.as_deref_mut().unwrap().key;
            assert_eq!(left, 3);
        }
        bt.replace_left_node(6);
        {
            let left = bt.left.as_deref_mut().unwrap().key;
            assert_eq!(left, 6);
        }
    }

    trait MyAsMut<T> {
        fn my_as_mut(&mut self) -> Option<&mut T>;
        fn my_as_mut1(&mut self) -> Option<&mut T>;
    }
    trait MyAsMut1<T, U> {
        fn my_and_then(self, f: impl FnOnce(T) -> Option<U>) -> Option<U>;
    }

    impl<T> MyAsMut<T> for Option<T> {
        fn my_as_mut(&mut self) -> Option<&mut T> {
            match *self {
                Some(ref mut v) => Some(v),
                None => None,
            }
        }

        fn my_as_mut1(&mut self) -> Option<&mut T> {
            match self {
                Some(v) => Some(v),
                None => None,
            }
        }
    }
    impl<T, U> MyAsMut1<T, U> for Option<T> {
        fn my_and_then(self, f: impl FnOnce(T) -> Option<U>) -> Option<U> {
            match self {
                Some(x) => f(x),
                None => None,
            }
        }
    }

    #[test]
    fn as_ref_test() {
        let mut p = Some(2);
        let pr = p.my_as_mut();
        println!("{:?}", pr);
        let pr1 = p.my_as_mut1();
        println!("{:?}", pr1);
    }

    struct S {
        foo: u64,
        bar: i64,
        _padding: [u8; 200 * 1024]
    }

    impl S {
        fn new() -> S {
            S { foo: 0, bar: 0, _padding: [0; 200 * 1024] }
        }
    }

    impl Drop for S {
        fn drop(&mut self) {
            println!("dropping S, foo: {}, bar: {}", self.foo, self.bar);
        }
    }

    #[test]
    fn let_test() {
        let v: Option<S> = None;
        let s = mem::size_of_val(&v);
        println!("what size: {}", s);
        println!("S size: {}", mem::size_of_val(&S::new()));
        drop(v);
        println!("sizeof option: {}", mem::size_of::<Option<S>>());
        struct Inner {}
        // 1
        println!("sizeof empty struct option: {}", mem::size_of::<Option<Inner>>());
        // 需要字节对齐
        struct Inner1 { _foo: u32 }
        // 4 + 4
        println!("sizeof Inner1 option: {}", mem::size_of::<Option<Inner1>>());

        struct Inner2 { _foo: u16 }
        // 2 + 2
        println!("sizeof Inner2 option: {}", mem::size_of::<Option<Inner2>>());
    }


    #[test]
    fn get_or_insert_with_test() {
        // 除非显示
        let mut p = Box::new(Some(S::new()));
        loop {
            let pi = p.as_mut();
            *pi = None;
            p.get_or_insert_with(|| S::new());
            println!("tick");
            thread::sleep(Duration::from_secs(1));
        }
    }
}
