use bytes::{BytesMut, Buf, BufMut};
use serde_json::Deserializer;
use tokio::{net::TcpStream, io::{BufWriter, AsyncReadExt, AsyncWriteExt}};
use tracing::debug;

use crate::{common::{Response, Request}, KvsError, Result};

/// inspired by mini-redis
#[derive(Debug)]
pub struct Connection {
    stream: BufWriter<TcpStream>,
    buffer: BytesMut,
}

impl Connection {
    pub fn new(socket: TcpStream) -> Connection {
        Connection {
            stream: BufWriter::new(socket),
            buffer: BytesMut::with_capacity(16),
        }
    }

    pub async fn read_resp(&mut self) -> Result<Response> {
        loop {
            if let Some(resp) = self.parse_resp()? {
                return Ok(resp);
            }

            if 0 == self.stream.read_buf(&mut self.buffer).await? {
                return Err(KvsError::Any(anyhow::anyhow!("connection reset by peer")));
            }
        }
    }

    pub async fn read_req(&mut self) -> Result<Request> {
        loop {
            debug!(">>>>>> before parse request, buffer cap: {}, remaining_mut: {}, remaining: {}", self.buffer.capacity(), self.buffer.remaining_mut(), self.buffer.remaining());
            if let Some(req) = self.parse_req()? {
                debug!(">>>>>> parse request complete");
                return Ok(req);
            }
            debug!(">>>>>> no enough bytes in buffer, read from socket");

            let read_count = self.stream.read_buf(&mut self.buffer).await?;
            if 0 == read_count {
                debug!(">>>>>> read socket error");
                return Err(KvsError::Any(anyhow::anyhow!("connection reset by peer")));
            }
            debug!(">>>>>> read {} bytes from socket", read_count);
        }
    }

    pub async fn write_json(&mut self, json: &str) -> Result<()> {
        self.stream.write_all(json.as_bytes()).await?;
        self.stream.flush().await.map_err(|e| KvsError::from(e))
    }

    fn parse_resp(&mut self) -> Result<Option<Response>> {
        let slice = &self.buffer[..];
        let mut json_stream = Deserializer::from_slice(slice).into_iter::<Response>();

        match json_stream.next() {
            None => Ok(None),
            Some(Ok(resp)) => {
                let offset = json_stream.byte_offset();
                self.buffer.advance(offset);
                Ok(Some(resp))
            }
            Some(Err(e)) => {
                if e.is_eof() {
                    Ok(None)
                } else {
                    Err(KvsError::Serde(e))
                }
            }
        }
    }

    fn parse_req(&mut self) -> Result<Option<Request>> {
        let mut json_stream = Deserializer::from_slice(&self.buffer[..]).into_iter::<Request>();
        match json_stream.next() {
            None => Ok(None),
            Some(Ok(req)) => {
                let offset = json_stream.byte_offset();
                self.buffer.advance(offset);
                Ok(Some(req))
            }
            Some(Err(e)) => {
                if e.is_eof() {
                    Ok(None)
                } else {
                    Err(KvsError::Serde(e))
                }
            }
        }
    }

}
