#![deny(missing_docs)]
#![feature(new_uninit)]
//! A simple key/value store.

pub use client::KvsClient;
pub use engines::{KvStore, KvsEngine, SledKvsEngine};
pub use error::{KvsError, Result};
pub use data_struct::{SafeDeque, UnsafeList, LibVec, Arc, shieber};

mod client;
mod common;
mod engines;
mod error;
pub mod server;
pub mod thread_pool;
mod connection;
mod data_struct;
/// interactive with naive library and C header file
pub mod ffi_test;
pub mod util;
pub mod config;


