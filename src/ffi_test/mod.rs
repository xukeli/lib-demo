#![allow(non_camel_case_types)]
#![allow(non_snake_case)]
#![allow(missing_docs)]
extern crate libc;
// use libc::c_int;

// #[link(name = "c_api")]
// extern "C" {
//     /// test 
//     pub fn add(a: c_int, b: c_int) -> c_int;
//     /// 回调注册
//     pub fn register_callback(target: *mut RustObject, cb: extern fn(*mut RustObject, i32)) -> i32;
//     /// 回调执行
//     pub fn trigger_callback();
// }

#[repr(C)]
pub struct RustObject {
    pub a: i32,
    pub b: u64,
}

/// 这里的extern表示该函数在C中被调用
pub extern "C" fn callback(target: *mut c_void, a: i32) {
    println!("I'm called from C with value {0}", a);
    unsafe {
        (*(target as *mut RustObject)).a = a;
    }
}

/// 链接到C
#[no_mangle]
pub extern "C" fn another(_target: *mut RustObject, a: i32) {
    println!("I'm anoher linked called from C with value {0}", a);
}

pub mod bindings;
pub use bindings::*;
use libc::c_void;
