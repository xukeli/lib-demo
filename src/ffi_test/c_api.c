#include "c_api.h"

int add(int a, int b) {
    return a + b;
}

void *cb_target;
rust_callback cb;

int register_callback(void *callback_target, rust_callback callback) {
    cb_target = callback_target;
    cb = callback;
    return 1;
}

void trigger_callback() {
    cb(cb_target, 7);
    // 直接调用链接的函数
    another(cb_target, 114514);
}