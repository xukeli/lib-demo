int add(int a, int b);

typedef void (*rust_callback)(void *, int);

int register_callback(void *callback_target, rust_callback callback);
void trigger_callback();

typedef struct CStructS {
    double a;
    long b;
    int c;
    unsigned int d;
    unsigned long e;
} CStructT;

extern void another(void *, int);
