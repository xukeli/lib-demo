use kvs::ffi_test::{add, callback, register_callback, trigger_callback, RustObject};
use libc::c_void;

fn main() {
    println!("before call c function");
    let ret = unsafe { add(1, 2) };
    println!("ffi add ret: {}", ret);

    let mut rust_object = Box::new(RustObject { a: 5, b: 6 });
    println!("before register callback function");
    unsafe {
        register_callback(&mut *rust_object as *mut RustObject as *mut c_void, Some(callback));
        trigger_callback();
    }
}
