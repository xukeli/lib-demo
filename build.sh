#!/bin/bash

# build so file
echo "build..."
cd src/ffi_test
gcc -shared -fPIC -o libc_api.so c_api.c
cd ../../
cargo build
