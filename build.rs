use std::{path::Path, fs, process::Command};

extern crate pkg_config;

fn main() {
    println!("cargo:rustc-link-lib=c_api");
    println!("cargo:rustc-link-search=src/ffi_test");
    println!("cargo:rustc-link-arg=-Wl,-rpath,$ORIGIN/lib");

    let profile = std::env::var("PROFILE").unwrap();
    let manifest_dir = std::env::var("CARGO_MANIFEST_DIR").unwrap();

    let lib_finename = "libc_api.so";

    let lib_from_dir_path = Path::new(&manifest_dir)
        .join("src/ffi_test");
    let lib_from_path = lib_from_dir_path.join(lib_finename);
    if !lib_from_path.exists() {
        Command::new("gcc")
            .args(&["-shared", "-fPIC", "-o"])
            .arg(lib_from_path.as_os_str())
            .arg(format!("{}/c_api.c", lib_from_dir_path.to_str().unwrap()))
            .status().unwrap();
    }

    let lib_to_dir_path = Path::new(&manifest_dir)
        .join("target")
        .join(profile.as_str())
        .join("lib");
    let lib_to_path = lib_to_dir_path.clone().join(lib_finename);

    if !lib_to_dir_path.exists() {
        fs::create_dir(lib_to_dir_path).unwrap();
    }

    fs::copy(lib_from_path, lib_to_path).unwrap();
}